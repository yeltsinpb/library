<?php

namespace Tests\Feature;

use App\Book;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookReservationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_add_a_book()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/books',[
            'title'     =>  'Summer Season Book',
            'author'    =>  'John Smith',
        ]);

        $response->assertOk();
        $this->assertCount(1,Book::all());
    }

    /** @test */
    public function title_is_required()
    {      

        $response = $this->post('/books',[
            'title'     =>  '',
            'author'    =>  'John Smith',
        ]);

        $response->assertSessionHasErrors('title');
    }

    /** @test */
    public function author_is_required()
    {      
        
        $response = $this->post('/books',[
            'title'     =>  'Summer Season Book',
            'author'    =>  '',
        ]);

        $response->assertSessionHasErrors('author');
    }

    /** @test */
    public function can_update_a_book()
    {
        $this->withoutExceptionHandling();

        $this->post('/books',[
            'title'     =>  'Summer Season Book',
            'author'    =>  'John Smith',
        ]);

        $book = Book::first();
        $response = $this->patch('/books/' . $book->id,[
            'title'     => 'Updated Title',
            'author'    => 'Updated Author',
        ]);

        $this->assertEquals('Updated Title',Book::first()->title);
        $this->assertEquals('Updated Author',Book::first()->author);

    }
}
